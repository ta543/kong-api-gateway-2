local typedefs = require "kong.db.schema.typedefs"

return {
    name = "custom-logger",
    fields = {
        { consumer = typedefs.no_consumer },
        {
            config = {
                type = "record",
                fields = {
                    { log_level = { type = "string", default = "debug", one_of = { "debug", "info", "notice", "warn", "error", "crit", "alert", "emerg" }, }, },
                },
            },
        },
    },
    entity_checks = {
    },
}
