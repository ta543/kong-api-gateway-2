local CustomLoggerHandler = {
    PRIORITY = 1000,
    VERSION = "0.1",
}

function CustomLoggerHandler:access(config)
    kong.log.debug("Executing custom logger access phase!")
end

return CustomLoggerHandler
