const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);

describe('Tests for product-service', function() {
  it('should connect to the product-service API', (done) => {
    chai.request('http://localhost:3000')
        .get('/')
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('string');
          expect(res.body).to.equal('Hello from product-service!');
          done();
        });
  });
});
